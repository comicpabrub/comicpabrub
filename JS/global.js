var myNav = document.getElementById('better-header');
var dropdown = document.getElementsByClassName('gb-dropdown-content')[0];
var shouldAnimate = false;

window.onscroll = function () { 
    "use strict";

    if (document.documentElement.scrollTop >= document.getElementById('wrapper').clientHeight - 100) {
        myNav.classList.add("nav-colored");
        myNav.classList.remove("nav-transparent");

        dropdown.style.backgroundColor = "rgb(9, 92, 122)";
        shouldAnimate = true;
    } 
    else if (shouldAnimate) {        
        myNav.classList.add("nav-transparent");
        myNav.classList.remove("nav-colored");

        dropdown.style.background = "rgba(0, 0, 0, 0.377)";                
    }
};

var links = document.getElementsByClassName('list-link');
var auxNav = document.getElementById('main-header');

if (auxNav == null) {
    auxNav = document.getElementById('better-header');    
}

for (var i = 0; i < links.length; i++) {
    links[i].style.lineHeight = auxNav.clientHeight + "px"; 
}