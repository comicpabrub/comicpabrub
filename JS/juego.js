var stage;
var grupo;
var fallos = 0;

var autores = [
    {image:"images/juego/pr_2.jpg", name:"paco"}, 
    {image:"images/juego/sg_1.jpg", name:"santiago"}, 
    {image:"images/juego/fi.jpg", name:"francisco"}, 
    {image:"images/juego/dr.jpg", name:"david"}
    ]

var comics = [
    {image:"images/juego/arrugas.jpg", name:"arrugas-paco"}, 
    {image:"images/juego/mortadelofilemon.jpg", name:"mortadelofilemon-francisco"}, 
    {image:"images/juego/meninas.jpg", name:"meninas-santiago"}, 
    {image:"images/juego/elheroe.jpg", name:"elheroe-david"}, 
    {image:"images/juego/lighthouse.jpg", name:"lighthouse-paco"},
    {image:"images/juego/circo.jpg", name:"circo-david"}, 
    {image:"images/juego/latempestad.jpg", name:"latempestad-santiago"},
    ]

var previousX = -250;
var cpreviousX = -130;

function inicializar() 
{    
    stage = new createjs.Stage('escenario');

    texto = new createjs.Text('0 fallos', "20px Arial", "white"); 
    texto.x = 10;
    texto.y = 10;
    stage.addChild(texto);
    stage.update();

    texto2 = new createjs.Text('Resetear', "20px Arial", "white"); 
    texto2.x = stage.canvas.width - 10 - texto2.getMeasuredWidth();
    texto2.y = 10;
    texto2.addEventListener('click', function() {
        resetear();
    });
    stage.addChild(texto2);
    stage.update();

    textoEstado = new createjs.Text('Arrastra cada obra a su autor', "20px Arial", "white"); 
    textoEstado.x = stage.canvas.width/2;
    textoEstado.y = 10;
    textoEstado.textAlign = "center";
    stage.addChild(textoEstado);
    stage.update();

   loadAuthor(autores, 0);
}

function loadAuthor(array, index) {
    if (array.length != index) {
        var autor = new Image();
        autor.src = array[index].image;
        autor.name = array[index].name;

        autor.onload = function(event) {
            var bitmap = new createjs.Bitmap(event.target);
            bitmap.x = previousX + 250;
            bitmap.y = 40;
            bitmap.name = this.name;
            previousX = bitmap.x;

            allowDetection(bitmap);

            stage.addChild(bitmap);
            stage.update();

            loadAuthor(array, index+1);
        }
    } else {
        loadComic(comics, 0);
    }
}

function loadComic(array, index) {
    console.log(index);
    console.log(array);
    if (array.length != index) {
        var comic = new Image();
        comic.src = array[index].image;
        comic.name = array[index].name;

        comic.onload = function(event) {
            var bitmap = new createjs.Bitmap(event.target);
            bitmap.x = cpreviousX + 140;
            bitmap.y = stage.canvas.height-200;
            bitmap.name = this.name;
            bitmap.originalX = bitmap.x;

            cpreviousX = bitmap.x;

            addListener(bitmap);

            stage.addChildAt(bitmap, 7);
            stage.update();

            loadComic(array, index+1);
        }
    }
}

function comprobarRespuesta(target) {
    targets = stage.getObjectsUnderPoint(stage.mouseX, stage.mouseY);
    if (targets.length != 2) return;
    if (targets[1].name.split("-").length == 2) return;

    if (targets[0].name.split("-")[1] == targets[1].name) {
        console.log("BIEN");

        textoEstado.text = "¡Correcto!";
        textoEstado.color = "lightgreen";

        target.removeAllEventListeners();
        stage.removeChild(target);
        stage.update();     
    } else {
        console.log("MAL");

        textoEstado.text = "Incorrecto";
        textoEstado.color = "red";

        target.x = target.originalX;
        target.y = stage.canvas.height-200;
        fallos++;
    }

    if (typeof timeout !== "undefined") {
        clearTimeout(timeout);
    }

    timeout = setTimeout(removeText, 3000);
    updateText();
}

function removeText() {
    textoEstado.text = "";
    stage.update();
}

function updateText() {
    texto.text = fallos + " fallo" + (fallos != 1 ? "s" : "");
    stage.update();
}

function resetear() {
    stage.removeAllChildren();
    previousX = -250;
    cpreviousX = -130;
    fallos = 0;
    stage.update();
    inicializar();
}

function allowDetection(target) {
    target.addEventListener('pressup', stopDrag); // Escucha la liberación del botón del ratón
} 

function addListener(target) {
    target.addEventListener('mousedown', startDrag); // Escucha la pulsación del botón del ratón
    target.addEventListener('pressmove', drag);   // Escucha movimiento del ratón, después de haber sido pulsado
    target.addEventListener('pressup', stopDrag); // Escucha la liberación del botón del ratón
}

function startDrag(event) {
	// Esta función sirve para que al arrastrar no se haga desde el punto central del grupo, 
	// sino desde aquel punto donde se hizo click
    var grupo = event.currentTarget;
    var offset = grupo.globalToLocal(event.stageX, event.stageY);
    
	// Se evitaran problemas si el grupo estuviera escalado
    grupo.offsetX = offset.x * grupo.scaleX; 
    grupo.offsetY = offset.y * grupo.scaleY;
}

function drag(event) {
    var grupo = event.currentTarget;

	// La nueva posicion se calcula como las coordenadas en pantalla donde se hizo click menos
	// el desfase (dentro del grupo) calculado en la funcion startDrag
    grupo.x = event.stageX - grupo.offsetX;
    grupo.y = event.stageY - grupo.offsetY;
    stage.update();
}

function stopDrag(event) {
    comprobarRespuesta(event.target);
}