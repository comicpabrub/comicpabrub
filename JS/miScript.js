function iniciar(){
	miVideo=document.getElementById("miPelicula"); 
	reproducir= document.getElementById("botonPlay");
  audio = document.getElementById("botonAudio");
  parar=document.getElementById("botonStop");
 	barra= document.getElementById("barra");
 	barraProgreso= document.getElementById("barraProgreso");

	barra.largoBarra=530;
  reproducir.addEventListener("click", playOPausa,false);
  audio.addEventListener("click", sonar,false);
  parar.addEventListener("click", stop, false);
  barra.addEventListener("click", clickEnBarra,false);
}

function playOPausa(){
  if(!miVideo.paused && !miVideo.ended){
    miVideo.pause();
    reproducir.src="images/video/play1.png";
    window.clearInterval(actualizarBarra);
  }
  else{
    miVideo.play();
    reproducir.src="images/video/pausa1.png";
    actualizarBarra= setInterval(actualizar, 500);
  }
}

function clickEnBarra(evento){
  if(!miVideo.paused && !miVideo.ended){
   var ratonX=evento.pageX - barra.offsetLeft;
    miVideo.currentTime=miVideo.duration*ratonX/barra.largoBarra;
    barraProgreso.style.width=ratonX+"px";
  }
}

function stop(){
 	miVideo.pause();
  barraProgreso.style.width="0px";
  miVideo.currentTime=0;
  reproducir.src="images/video/play1.png";
}

function sonar(){
 if(miVideo.muted){
 		miVideo.muted = false;
 		audio.src="images/video/audiosi1.png";
 }
 else{
   miVideo.muted=true;
   audio.src="images/video/audiono1.png";
 
 }
}

function actualizar(){
 if(!miVideo.ended){
   	var largo=parseInt(miVideo.currentTime/miVideo.duration * barra.largoBarra);
    barraProgreso.style.width=largo+"px";
	}
  else{
  	barraProgreso.style.width="0px";
    reproducir.src="images/video/play1.png";
    window.clearInterval(actualizarBarra);
  }
}

window.addEventListener("load",iniciar,false);